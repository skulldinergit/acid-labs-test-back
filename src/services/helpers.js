import constants from './constants';

export default class {
    static proccessWeatherError (errorCode, errorResponse={}) {
        
        if (errorCode === constants.TIMEOUT_ERROR) {
            return {statusCode: 408, error: 'Timeout en la conexión al servidor'};
        }

        if (errorResponse.status === 400) {
            return { statusCode: errorResponse.status, error: 'No se ha encontrado un lugar válido para los datos ingresados' };
        }

        return { statusCode: 500, error: 'Error interno en el servidor' };
    }
}