import axios from 'axios';
import constants from './constants';
import helpers from './helpers';

export const getWeather = async({ latitude, longitude}) => {
    try {
        const url = `${constants.URI_WEATHER}/${constants.URI_API_KEY}/${latitude},${longitude}?${constants.URI_STATIC_PARAMS}` ;
        const response = await axios(url, {
            method: 'get',
            timeout: constants.TIMEOUT, 
        });

        if (response.statusText === 'OK'){
            return { statusCode: 200, data: response.data};
        }

        return helpers.proccessWeatherError(500, {});
    } catch (error) {
        // console.log("Error en API postClientOrder: ", error.code, error.response.status);
        return helpers.proccessWeatherError(error.code, error.response);
    }
};