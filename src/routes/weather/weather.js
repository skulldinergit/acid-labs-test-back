import express from 'express';
import { getWeather } from '../../services/weatherService';
import redis from 'redis';
import { promisify } from 'util';
import constants from './constants';

var router = express.Router();

router.get('', async (req, res, next) => {
    console.log("weather get service");
    const { latitude, longitude } = req.query;
    let randomError = false;
    let weatherResponse;

    // add a 10% of random error
    do {
        randomError = false;
        
        if (Math.random() < 0.1) {
            console.log("random error detected!");
            randomError = true;
        }
    } while (randomError === true);

    weatherResponse = await getWeather({ latitude, longitude });

    // check response for handled errors
    if (weatherResponse.error) {
        const { statusCode, error } = weatherResponse;

        return res.status(statusCode).jsonp({ error });
    }

    // set the response to res
    res.responseWeather = weatherResponse;

    next();
}, async (req, res, next) => {
    // insert into redis


    try {
        const { data } = res.responseWeather;
        const { AUTOINC_KEY } = constants;

        const client = redis.createClient(process.env.REDIS_URL);
        const getAsync = promisify(client.get).bind(client);

        // Catch errors on redis and notify it on response
        client.on('error', (err) => {
            console.log("redis error "), err;
            client.quit();
            client.end(true);
        });

        // await console.log(await getAsync('weather1'));
        // get latest key
        client.incr(AUTOINC_KEY);
        const autoincKey = await getAsync(AUTOINC_KEY);

        // save data to redis with a new key
        client.set(`weather${autoincKey}`, JSON.stringify(data), 'EX', 240);
        client.quit();
    } catch (error) {
        console.log(error);
        res.redisError = { error: 'Error en redis' };
    }

    next();
}, async (req, res) => {
    // final response for full flux
    const { statusCode, data } = res.responseWeather;
    const { error } = res.redisError || {};
    res.status(statusCode).jsonp({ data, redisError: error });
});

export default router;