import express from 'express';
import cors from 'cors';
import weatherRouter from './routes/weather/weather';

const app = express();
const port = process.env.PORT || 3000;

app.use(cors({ origin: true }));
app.use('/weather', weatherRouter);

app.listen(port, () => console.log(`App listening on port ${port}`));