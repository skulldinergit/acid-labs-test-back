FROM node:10.15.3-alpine

WORKDIR /usr/app

COPY package.json .

RUN npm i --quiet

COPY . .

EXPOSE 3000

# RUN npm install pm2 -g

CMD ["npm", "start"]