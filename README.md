# acid-labs-test-back

A node with expressjs app and redis

# Funcionamiento
 - Inciar app con ```npm start```

# Deploy en docker
 - Correr el comando ```docker-compose up``` el cual levanta redis y la app en nodejs en el puerto 80

# AWS
 - La app corre sobre AWS en la URL ```http://ec2-54-233-129-141.sa-east-1.compute.amazonaws.com```